## Introduction to Python

Jupyter Notebook and supporting files for an introduction to programming with Python.

1. Introduction: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/davecg%2Fintroduction-to-python/master?filepath=Introduction%20to%20Python.ipynb)

1. Plotting and Intro to Sklearn [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/davecg%2Fintroduction-to-python/master?filepath=Plotting%20and%20Introduction%20to%20Scikit-Learn.ipynb)